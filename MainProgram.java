package greatLearning.assignment3;

import java.util.Scanner;

public class MainProgram {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s2=new Scanner(System.in);
		   MagicOfBooks mb=new MagicOfBooks();
		   
			while (true) {
				System.out.println("1. As an Admin, I can add a new book \r\n"
						+ "2. As an Admin, I can delete a book\r\n" + "3. As an Admin, I can update a book\r\n"
						+ "4. As an Admin, I can display all the books \r\n" + "5. As an Admin, I can see the total count of the book \r\n"
						+ "6. As an Admin, I can see the all the books under Autobiography genre" +  "\n7. As an Admin, I can arrange the book in the following order");
	       
				System.out.println("enter your choice:");
				int choice = s2.nextInt();

				switch (choice) {

				case 1: System.out.println("Please Enter Number of Books you want to add");
				        int a=s2.nextInt();
					for(int i=1;i<=a;i++) {
						mb.addbook();
						}
					break;
				case 2: 
					    mb.deletebook();
					    break;
		
				case 3:
					   mb.updatebook();
					   break;

				case 4:  
						mb.displayBookInfo();
						break;
				
				case 5: 
						System.out.println("Count of all books-");
						mb.count();
						break;
					
				case 6: 
						mb.autobiography();
						break;
				      
				
				case 7: 

								System.out.println("Choose your choice:\n 1. Price low to high "
													+ "\n 2.Price high to low \n 3. Best selling");
								int ch = s2.nextInt();
								switch (ch) {

									case 1 : mb.displayByFeature(1);
											break;
									case 2: mb.displayByFeature(2); 
											break;
									case 3: mb.displayByFeature(3);
											break;
									}
				
								default:
									System.out.println(" You have entered wrong choice!!!!");

				}

			}

	     }
				
		

	}


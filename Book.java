package greatLearning.assignment3;

public class Book {
	String Name;
	double Price;
	String Genre;
	int noOfCopiesSold;
	String bookstatus;
	int id;
	@Override
	public String toString() {
		return "Book [Name=" + Name + ", Price=" + Price + ", Genre=" + Genre + ", noOfCopiesSold=" + noOfCopiesSold
				+ ", bookstatus=" + bookstatus + ", id=" + id + "]";
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		Price = price;
	}
	public String getGenre() {
		return Genre;
	}
	public void setGenre(String genre) {
		Genre = genre;
	}
	public int getNoOfCopiesSold() {
		return noOfCopiesSold;
	}
	public void setNoOfCopiesSold(int noOfCopiesSold) {
		this.noOfCopiesSold = noOfCopiesSold;
	}
	public String getBookstatus() {
		return bookstatus;
	}
	public void setBookstatus(String bookstatus) {
		this.bookstatus = bookstatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}

package greatLearning.assignment3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class MagicOfBooks {
	ArrayList<Book> list =new ArrayList<>();
	HashMap<Integer, Book> h1 = new HashMap<>();
	TreeMap<Double, Book> t1 = new TreeMap<>();
	Scanner s1=new Scanner(System.in);
	public void addbook() {
		Book b1 = new Book();
		System.out.println("Enter a Book Id : ");
		b1.setId(s1.nextInt());
		System.out.println("Enter a Book Name : ");
		b1.setName(s1.next());
		System.out.println("Enter the Book Status : ");
		b1.setBookstatus(s1.next());
		System.out.println("Enter the Book Genre : ");
		b1.setGenre(s1.next());
		System.out.println("Enter the NumberOfCopiesSold : ");
		b1.setNoOfCopiesSold(s1.nextInt());
		System.out.println("New Book is added : " + h1.put(b1.getId(), b1));
		t1.put(b1.getPrice(), b1); list.add(b1);
	}
	 public void deletebook() {
	    	System.out.println("Enter Book Id that you want to delete it : ");
	    	int id=s1.nextInt();
	    	h1.remove(id);
	 }
	    
	    public void updatebook() {
	    	Book b2= new Book();
	    	System.out.println("Enter the Book Id : ");
	    	b2.setId(s1.nextInt());
	    	
			System.out.println("Enter a Book Name : ");
			b2.setName(s1.next());
			System.out.println("Enter a Book Price : ");
			b2.setPrice(s1.nextDouble());

			System.out.println("Enter a Book Genre : ");
			b2.setGenre(s1.next());
			
			System.out.println("Enter NumberOfCopiesSold : ");
			b2.setNoOfCopiesSold(s1.nextInt());
			
			System.out.println("Enter a Book Status : ");
			b2.setBookstatus(s1.next());
		
			System.out.println("Updated a Book details" + h1.replace(b2.getId(), b2));
			
		}
	    
		public void displayBookInfo() {
			
			if (h1.size() > 0) 
			{
				Set<Integer> keySet = h1.keySet();

				for (Integer key : keySet) {

					System.out.println(key + " ----> " + h1.get(key));
				}
			} else {
				System.out.println("BooksMap is Empty");
			}

		}
public void count() {
			
			System.out.println("Number of books present in a store"+ h1.size());
			
		}	
		
		public void autobiography() {
		       String bestSelling = "Autobiography";
	        
	        ArrayList<Book> genreBookList = new ArrayList<Book>();
		         
		         Iterator<Book> iter=(list.iterator());
		         
		         while(iter.hasNext())
		         {
		             Book b1=(Book)iter.next();
		             if(b1.Genre.equals(bestSelling)) 
		             {
		            	 genreBookList.add(b1);
		            	 System.out.println(genreBookList);
		             }
		         }  
		}
		
		public void displayByFeature(int flag) {
			
		if(flag==1)
		{
			if (t1.size() > 0) 
			{
			  Set<Double> keySet = t1.keySet();
   			  for (Double key : keySet) {
				System.out.println(key + " ----> " + t1.get(key));
   			  }
		} else {
					System.out.println("treeMap is Empty");
				}
	}
	if(flag==2) 
	{
		 Map<Double, Object> treeMapReverseOrder= new TreeMap<>(t1);
		  NavigableMap<Double, Object> nmap = ((TreeMap<Double, Object>) treeMapReverseOrder).descendingMap();
		      
		  System.out.println("Book Details:");
		      
		   if (nmap.size() > 0) 
		     {
				Set<Double> keySet = nmap.keySet();
					for (Double key : keySet) {
							System.out.println(key + " ----> " + nmap.get(key));
						}
				}else{
					System.out.println("treeMap is Empty");
				}
		
	 }
	
	if(flag==3) 
	{
	    String bestSelling = "B";
        ArrayList<Book> genreBookList = new ArrayList<Book>();
	         
	         Iterator<Book> itr = list.iterator();
	         
	         while(itr.hasNext())
	         {
	             Book b=(Book)itr.next();
	             if(b.bookstatus.equals(bestSelling)) 
	             {
	            	 genreBookList.add(b);
	            	
	             }
		         
              }
	         System.out.println(genreBookList);
	         
	   }
    }	
}
